import 'package:flutter/material.dart';

class FeedReview extends StatefulWidget {
  const FeedReview({Key? key}) : super(key: key);

  @override
  State<FeedReview> createState() => _FeedReviewState();
}

class _FeedReviewState extends State<FeedReview> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 30, right: 30, bottom: 15),
        child: Container(
            height: 80,
            decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6)
                ]),
            child: Padding(
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
              child: Row(children: [
                Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/Content/12.jpg"),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10)),
                ),
                SizedBox(width: 10),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      Text(
                        "Thailand Safe To Travel",
                        style: TextStyle(
                            fontFamily: 'Cordia New',
                            fontSize: 15,
                            color: const Color(0xff655c98),
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(
                          child: Text(
                        "4 hours ago",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontFamily: 'Segoe UI',
                            fontSize: 10,
                            color: const Color(0xff000000)),
                      ))
                    ]),
                    Flexible(
                        child: Container(
                            width: 150,
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "All the best parts of Thailand are constantly advertised. From the gorgeous landscapes and the delicious food to the smiling people and the enticing culture, Thailand seems like a magical travel destination. ",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              softWrap: false,
                              style: TextStyle(
                                  fontFamily: 'Cordia New',
                                  height: 1.3,
                                  fontSize: 13,
                                  color: const Color(0xff655c98)),
                            )))
                  ],
                )
              ]),
            )));
  }
}
