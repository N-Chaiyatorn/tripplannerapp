import 'package:flutter/material.dart';
import 'package:planner_demo/review_page/create_review.dart';
import 'package:planner_demo/review_page/feed_review.dart';

class ReviewPage extends StatefulWidget {
  const ReviewPage({Key? key}) : super(key: key);

  @override
  State<ReviewPage> createState() => _ReviewPageState();
}

class _ReviewPageState extends State<ReviewPage> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: Color(0xfff3f1fc),
          title: Text('LOGO',
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 25,
                  color: const Color(0xff655c98),
                  fontWeight: FontWeight.w700)),
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Container(
                height: 250,
                decoration: BoxDecoration(
                    color: const Color(0xfff3f1fc),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x38000000),
                          offset: Offset(0, 0),
                          blurRadius: 5)
                    ]),
                child: Column(
                  children: [
                    Padding(
                        padding: EdgeInsets.only(top: 30, left: 60, right: 60),
                        child: Container(
                            height: 50,
                            decoration: BoxDecoration(
                                color: const Color(0xffffffff),
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(20.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: const Color(0x29000000),
                                      offset: Offset(0, 3),
                                      blurRadius: 6)
                                ]),
                            child: TextFormField(
                                textAlign: TextAlign.start,
                                controller: _controller,
                                onChanged: (value) {
                                  setState(() {});
                                },
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  prefixIcon: Padding(
                                    padding: EdgeInsets.all(0),
                                    child: Icon(Icons.search,
                                        color: Color(0xff655c98)),
                                  ),
                                  hintText: "Search Review",
                                  hintStyle: TextStyle(
                                      fontFamily: 'Segoe UI',
                                      fontSize: 15,
                                      color: const Color(0xff655c98)),
                                  suffixIcon: _controller.text.isEmpty
                                      ? null
                                      : IconButton(
                                          icon: Icon(Icons.clear),
                                          padding:
                                              const EdgeInsets.only(top: 0),
                                          onPressed: () {
                                            setState(() {
                                              _controller.clear();
                                            });
                                          }),
                                )))),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 30, bottom: 10, left: 60, right: 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            child: Container(
                              height: 100,
                              width: 120,
                              decoration: BoxDecoration(
                                  color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                        color: const Color(0x29000000),
                                        offset: Offset(0, 3),
                                        blurRadius: 6)
                                  ]),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image(
                                      image: AssetImage(
                                          'assets/images/comment.png'),
                                      height: 40,
                                      color: Colors.black.withOpacity(0.57)),
                                  SizedBox(height: 10),
                                  Text(
                                    "Create Review",
                                    style: TextStyle(
                                        fontFamily: 'Segoe UI',
                                        fontSize: 15,
                                        color: const Color(0xff655c98)),
                                  )
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CreateReview()));
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              height: 100,
                              width: 120,
                              decoration: BoxDecoration(
                                  color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                        color: const Color(0x29000000),
                                        offset: Offset(0, 3),
                                        blurRadius: 6)
                                  ]),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image(
                                      image:
                                          AssetImage('assets/images/like.png'),
                                      height: 40,
                                      color: Colors.black.withOpacity(0.57)),
                                  SizedBox(height: 10),
                                  Text(
                                    "Favourite Review",
                                    style: TextStyle(
                                        fontFamily: 'Segoe UI',
                                        fontSize: 15,
                                        color: const Color(0xff655c98)),
                                  )
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CreateReview()));
                            },
                          )
                        ],
                      ),
                    )
                  ],
                )),
            Padding(
              padding:
                  EdgeInsets.only(top: 50, left: 45, right: 60, bottom: 30),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "NEWS",
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        fontSize: 10,
                        color: const Color(0xff655c98),
                        fontWeight: FontWeight.w700,
                        shadows: [
                          Shadow(
                              color: const Color(0x29000000),
                              offset: Offset(0, 3),
                              blurRadius: 6)
                        ]),
                  )),
            ),
            FeedReview(),
            FeedReview(),
            FeedReview(),
            FeedReview()
          ],
        )));
  }
}
