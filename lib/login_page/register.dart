import 'package:flutter/material.dart';
import 'package:planner_demo/login_page/form/buttonform.dart';
import 'package:planner_demo/login_page/form/calendarbox.dart';
import 'package:planner_demo/login_page/form/checkbox.dart';
import 'package:planner_demo/login_page/form/fillform.dart';
import 'package:planner_demo/login_page/form/genderdropdown.dart';
import 'package:planner_demo/login_page/form/passwordform.dart';
import 'package:planner_demo/login_page/information/privacy.dart';
import 'package:planner_demo/login_page/information/terms.dart';
import 'package:planner_demo/login_page/login.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String? username, email, birthday, gender;

  //TextController to read text entered in text field
  TextEditingController password = TextEditingController();
  TextEditingController confirmpassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xAA5f5496).withOpacity(0.8),
      body: SingleChildScrollView(
          child: Stack(children: [
        Image.asset("assets/images/Log In Background.png"),
        Column(children: [
          //Heading with Logo
          Container(
              height: 175,
              width: 400,
              decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(1000.0),
                      bottomRight: Radius.circular(1000.0))),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "LOGO",
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 65,
                      color: const Color(0xffbbb3e9),
                      fontWeight: FontWeight.w700),
                ),
              )),

          SizedBox(
            height: 80,
          ),

          //SIGN UP HEADER
          Text(
            "SIGN UP",
            style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 20,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700),
          ),

          SizedBox(
            height: 30,
          ),

          //USERNAME
          FillForm(name: "USERNAME"),

          SizedBox(
            height: 20,
          ),

          //EMAIL
          FillForm(name: "EMAIL"),

          SizedBox(
            height: 20,
          ),

          //PASSWORD
          PasswordForm(name: "PASSWORD"),

          SizedBox(
            height: 20,
          ),

          //CONFIRM PASSWORD
          PasswordForm(name: "CONFIRM PASSWORD"),

          SizedBox(
            height: 20,
          ),

          //BIRTHDAY
          CalendarBox(),

          SizedBox(
            height: 20,
          ),

          //Gender
          GenderDropdown(),

          SizedBox(
            height: 20,
          ),

          //Checkbox Agreement
          SizedBox(
              height: 40,
              width: 215,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CheckBox(),
                  Flexible(
                      child: Wrap(
                    children: [
                      GestureDetector(
                        child: Text("I accept the ",
                            style: TextStyle(
                                fontSize: 10,
                                color: const Color(0xffffffff),
                                fontWeight: FontWeight.w700)),
                      ),
                      GestureDetector(
                          child: Text("Terms of Uses ",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  decorationThickness: 2,
                                  fontFamily: 'Segoe UI',
                                  fontSize: 10,
                                  color: const Color(0xffffffff),
                                  fontWeight: FontWeight.w700)),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TermsDetail()));
                          }),
                      Text("and have read the ",
                          style: TextStyle(
                              fontFamily: 'Segoe UI',
                              fontSize: 10,
                              color: const Color(0xffffffff),
                              fontWeight: FontWeight.w700)),
                      GestureDetector(
                          child: Text("Privacy Policy",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  decorationThickness: 2,
                                  fontFamily: 'Segoe UI',
                                  fontSize: 10,
                                  color: const Color(0xffffffff),
                                  fontWeight: FontWeight.w700)),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PrivacyDetail()));
                          })
                    ],
                  ))
                ],
              )),

          SizedBox(
            height: 20,
          ),

          //SIGN UP
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            GestureDetector(
              child: ButtonForm(name: "SIGN UP"),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => LogIn()));
              },
            ),
          ]),

          SizedBox(
            height: 20,
          ),

          //Alrady have an account, back to login
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Already have an account?",
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 10,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700)),
              SizedBox(
                width: 2,
              ),
              InkWell(
                child: Text(
                  "Log In",
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 10,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                      decoration: TextDecoration.underline,
                      decorationThickness: 2),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LogIn()));
                },
              )
            ],
          ),
          SizedBox(
            height: 50,
          )
        ])
      ])),
    );
  }
}
