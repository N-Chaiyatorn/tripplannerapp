import 'package:flutter/material.dart';

class GenderDropdown extends StatefulWidget {
  const GenderDropdown({Key? key}) : super(key: key);

  @override
  State<GenderDropdown> createState() => _GenderDropdownState();
}

class _GenderDropdownState extends State<GenderDropdown> {
  String? dropdownvalue;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30,
        width: 200,
        decoration: BoxDecoration(
            color: const Color(0xffffffff),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: Center(
            child: DropdownButtonHideUnderline(
          child: DropdownButton(
              isExpanded: true,
              hint: Center(
                  child: Text(
                "GENDER",
                style: TextStyle(
                    fontFamily: 'Segoe UI',
                    fontSize: 15,
                    color: Color(0xffdfdfdf),
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              )),
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 15,
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontWeight: FontWeight.w700),
              value: dropdownvalue,
              icon:
                  Visibility(child: Icon(Icons.arrow_downward), visible: false),
              items: <String>['Male', 'Female', 'Other']
                  .map((String selectitem) => DropdownMenuItem(
                      value: selectitem,
                      child: Center(
                          child: Text(
                        selectitem,
                        textAlign: TextAlign.center,
                      ))))
                  .toList(),
              onChanged: (String? newValue) {
                setState(() {
                  dropdownvalue = newValue!;
                });
              }),
        )));
  }
}
