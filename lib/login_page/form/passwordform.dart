import 'package:flutter/material.dart';

class PasswordForm extends StatefulWidget {
  final String name;
  const PasswordForm({Key? key, required this.name}) : super(key: key);

  @override
  State<PasswordForm> createState() => _PasswordFormState();
}

class _PasswordFormState extends State<PasswordForm> {
  bool _showPassword = false;
  final TextEditingController _controller = TextEditingController();

  void showpassword() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30,
        width: 200,
        decoration: BoxDecoration(
            color: const Color(0xffffffff),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: TextFormField(
            controller: _controller,
            onChanged: (value) {
              setState(() {});
            },
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: widget.name,
              hintStyle: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 15,
                  color: const Color(0xffdfdfdf),
                  fontWeight: FontWeight.w700),
              suffixIcon: _controller.text.isEmpty
                  ? null
                  : IconButton(
                      icon: Icon(_showPassword
                          ? Icons.visibility
                          : Icons.visibility_off),
                      padding: const EdgeInsets.only(top: 0),
                      onPressed: showpassword,
                    ),
            ),
            obscureText: !_showPassword));
  }
}
