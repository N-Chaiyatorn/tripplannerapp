import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class CalendarBox extends StatefulWidget {
  const CalendarBox({Key? key}) : super(key: key);

  @override
  State<CalendarBox> createState() => _CalendarBoxState();
}

class _CalendarBoxState extends State<CalendarBox> {
  DateTime dateTime = DateTime.now();
  String? _chosenDateTime;
  final DateFormat formatter = DateFormat.yMMMMd();

  get child => null;

  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 210,
              color: const Color(0xffffffff),
              child: Column(
                children: [
                  SizedBox(
                    height: 150,
                    child: CupertinoDatePicker(
                        initialDateTime: dateTime,
                        maximumDate: DateTime.now(),
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (val) {
                          setState(() {
                            dateTime = val;
                            final DateFormat formatter = DateFormat.yMMMMd();
                            _chosenDateTime = formatter.format(val);
                          });
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: const Text(
                      'OK',
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          fontSize: 15,
                          color: Color(0xff5f5496),
                          fontWeight: FontWeight.w700),
                    ),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
            child: Container(
                alignment: Alignment.center,
                height: 30,
                width: 200,
                decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20.0)),
                child: Text(
                    _chosenDateTime != null ? "$_chosenDateTime" : "BIRTHDAY",
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        fontSize: 15,
                        color: (_chosenDateTime != null
                            ? Color.fromARGB(255, 0, 0, 0)
                            : Color(0xffdfdfdf)),
                        fontWeight: FontWeight.w700))),
            onTap: () {
              _showDatePicker(context);
            }),
      ],
    );
  }
}
