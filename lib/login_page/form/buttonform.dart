import 'package:flutter/material.dart';

class ButtonForm extends StatefulWidget {
  final String name;
  const ButtonForm({Key? key, required this.name}) : super(key: key);

  @override
  State<ButtonForm> createState() => _ButtonFormState();
}

class _ButtonFormState extends State<ButtonForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30,
        width: 100,
        decoration: BoxDecoration(
            color: const Color(0xffffffff),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: Center(
          child: Text(
            widget.name,
            style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 15,
                color: const Color(0xff5f5496),
                fontWeight: FontWeight.w700),
          ),
        ));
  }
}
