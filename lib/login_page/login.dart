import 'package:flutter/material.dart';
import 'package:planner_demo/login_page/forgetpass.dart';
import 'package:planner_demo/login_page/form/buttonform.dart';
import 'package:planner_demo/login_page/form/fillform.dart';
import 'package:planner_demo/login_page/form/passwordform.dart';
import 'package:planner_demo/login_page/register.dart';
import 'package:planner_demo/home_page/tab_bottombar.dart';

class LogIn extends StatelessWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xAA5f5496).withOpacity(0.8),
      body: SingleChildScrollView(
          child: Stack(children: [
        Image.asset("assets/images/Log In Background.png"),
        Column(
          children: [
            //Heading with Logo
            Container(
                height: 175,
                width: 400,
                decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(1000.0),
                        bottomRight: Radius.circular(1000.0))),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    "LOGO",
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        fontSize: 65,
                        color: const Color(0xffbbb3e9),
                        fontWeight: FontWeight.w700),
                  ),
                )),

            SizedBox(
              height: 80,
            ),

            //LOG IN HEADER
            Text(
              "LOG IN",
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 20,
                  color: const Color(0xffffffff),
                  fontWeight: FontWeight.w700),
            ),

            SizedBox(
              height: 30,
            ),

            //USERNAME
            FillForm(name: "USERNAME/EMAIL"),

            SizedBox(
              height: 20,
            ),

            //PASSWORD
            PasswordForm(name: "PASSWORD"),

            SizedBox(
              height: 20,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  child: ButtonForm(name: "REGISTER"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Register()));
                  },
                ),

                SizedBox(
                  width: 2,
                ),

                //GO
                GestureDetector(
                  child: ButtonForm(name: "GO"),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TabBottomBar()));
                  },
                ),
              ],
            ),

            SizedBox(
              height: 10,
            ),

            //FORGET PASSWORD
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  child: SizedBox(
                      height: 30,
                      width: 120,
                      child: Center(
                          child: Text(
                        "FORGET PASSWORD?",
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            decorationThickness: 2,
                            fontFamily: 'Segoe UI',
                            fontSize: 10,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700),
                      ))),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgetPassword()));
                  },
                ),
                SizedBox(
                  width: 2,
                ),
                SizedBox(
                  height: 30,
                  width: 100,
                )
              ],
            )
          ],
        )
      ])),
    );
  }
}
