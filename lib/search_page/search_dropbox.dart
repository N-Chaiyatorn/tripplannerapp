import 'package:flutter/material.dart';

class SearchDropBox extends StatefulWidget {
  final String name;
  const SearchDropBox({Key? key, required this.name}) : super(key: key);

  @override
  State<SearchDropBox> createState() => _SearchDropBoxState();
}

class _SearchDropBoxState extends State<SearchDropBox> {
  String? dropdownvalue;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30,
        width: 120,
        decoration: BoxDecoration(
            color: const Color(0xffffffff),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: Center(
            child: DropdownButtonHideUnderline(
          child: DropdownButton(
              isExpanded: true,
              hint: Center(
                  child: Text(
                widget.name,
                style: TextStyle(
                    fontFamily: 'Segoe UI',
                    fontSize: 15,
                    color: Color(0xff655c98),
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              )),
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 15,
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontWeight: FontWeight.w700),
              value: dropdownvalue,
              items: <String>['Male', 'Female', 'Other']
                  .map((String selectitem) => DropdownMenuItem(
                      value: selectitem,
                      child: Center(
                          child: Text(
                        selectitem,
                        textAlign: TextAlign.center,
                      ))))
                  .toList(),
              onChanged: (String? newValue) {
                setState(() {
                  dropdownvalue = newValue!;
                });
              }),
        )));
  }
}
