import 'package:flutter/material.dart';

class SearchTrend extends StatefulWidget {
  const SearchTrend({Key? key}) : super(key: key);

  @override
  State<SearchTrend> createState() => _SearchTrendState();
}

class _SearchTrendState extends State<SearchTrend> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 15),
        child: Container(
            height: 50,
            decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6)
                ]),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Row(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Trending in Thailand",
                      style: TextStyle(
                          fontFamily: 'Cordia New',
                          fontSize: 10,
                          color: const Color(0xff655c98)),
                    ),
                    Text(
                      "#Bangkok",
                      style: TextStyle(
                          fontFamily: 'Cordia New',
                          fontSize: 15,
                          color: const Color(0xff655c98),
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                )
              ]),
            )));
  }
}
