import 'package:flutter/material.dart';

class SearchButton extends StatefulWidget {
  final String name;
  const SearchButton({Key? key, required this.name}) : super(key: key);

  @override
  State<SearchButton> createState() => _SearchButtonState();
}

class _SearchButtonState extends State<SearchButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30,
        width: 100,
        decoration: BoxDecoration(
            color: const Color(0xff5f5496),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: Center(
          child: Text(
            widget.name,
            style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 15,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700),
          ),
        ));
  }
}
