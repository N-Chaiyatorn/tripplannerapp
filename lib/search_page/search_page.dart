import 'package:flutter/material.dart';
import 'package:planner_demo/home_page/home_page.dart';
import 'package:planner_demo/search_page/search_button.dart';
import 'package:planner_demo/search_page/search_dropbox.dart';
import 'package:planner_demo/search_page/search_trend.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffe1ddf4),
        body: SingleChildScrollView(
            child: Column(children: [
          Container(
            alignment: Alignment.center,
            height: 300,
            decoration: BoxDecoration(
                color: const Color(0xfff2f2f2),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50)),
                boxShadow: []),
            child: Text(
              "LOGO",
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 109,
                  color: const Color(0xff655c98),
                  fontWeight: FontWeight.w700),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Column(children: [
            Padding(
                padding: EdgeInsets.only(left: 50, right: 50),
                child: Column(
                  children: [
                    Container(
                        height: 40,
                        decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(20.0),
                            boxShadow: [
                              BoxShadow(
                                  color: const Color(0x29000000),
                                  offset: Offset(0, 3),
                                  blurRadius: 6)
                            ]),
                        child: TextFormField(
                            textAlign: TextAlign.start,
                            controller: _controller,
                            onChanged: (value) {
                              setState(() {});
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Padding(
                                padding: EdgeInsets.all(0),
                                child: Icon(Icons.search,
                                    color: Color(0xff655c98)),
                              ),
                              hintText: "Search Location",
                              hintStyle: TextStyle(
                                  fontFamily: 'Segoe UI',
                                  fontSize: 20,
                                  color: const Color(0xff655c98)),
                              suffixIcon: _controller.text.isEmpty
                                  ? null
                                  : IconButton(
                                      icon: Icon(Icons.clear),
                                      padding: const EdgeInsets.only(top: 0),
                                      onPressed: () {
                                        setState(() {
                                          _controller.clear();
                                        });
                                      }),
                            ))),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SearchDropBox(
                          name: "Categories",
                        ),
                        SearchDropBox(name: "Location"),
                        GestureDetector(
                          child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              color: const Color(0xffffffff),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Icon(
                              Icons.location_on_outlined,
                              color: Color(0xff655c98),
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage()));
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    //Search Button
                    GestureDetector(
                      child: SearchButton(name: "SEARCH"),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()));
                      },
                    ),
                    SizedBox(height: 40),
                    //Trend Box
                    Container(
                        height: 332,
                        decoration: BoxDecoration(
                            color: const Color(0xfff2f2f2),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(children: [
                                SizedBox(width: 10),
                                Container(
                                    height: 20,
                                    child: Text("Trends for you",
                                        style: TextStyle(
                                            fontFamily: 'Segoe UI',
                                            fontSize: 17,
                                            color: const Color(0xff655c98),
                                            fontWeight: FontWeight.w700,
                                            shadows: [
                                              Shadow(
                                                color: const Color(0x29000000),
                                                offset: Offset(0, 3),
                                                blurRadius: 6,
                                              )
                                            ])))
                              ]),
                              SizedBox(height: 10),
                              SearchTrend(),
                              SearchTrend(),
                              SearchTrend(),
                              SearchTrend(),
                              Row(
                                children: [
                                  SizedBox(width: 10),
                                  GestureDetector(
                                    child: Text("Show more",
                                        style: TextStyle(
                                            fontFamily: 'Segoe UI',
                                            fontSize: 10,
                                            color: const Color(0xff655c98),
                                            shadows: [
                                              Shadow(
                                                color: const Color(0x29000000),
                                                offset: Offset(0, 3),
                                                blurRadius: 6,
                                              )
                                            ])),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  HomePage()));
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ))
                  ],
                )),
          ])
        ])));
  }
}
