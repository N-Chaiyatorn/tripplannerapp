import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:planner_demo/search_page/search_page.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class AdsBox extends StatefulWidget {
  const AdsBox({Key? key}) : super(key: key);

  @override
  State<AdsBox> createState() => _AdsBoxState();
}

class _AdsBoxState extends State<AdsBox> {
  int activeIndex = 0;
  final List AdsImage = [
    "assets/images/Content/11.jpg",
    "assets/images/Content/2.jpg",
    "assets/images/Content/4.jpg",
    "assets/images/Content/5.jpg",
    "assets/images/Content/10.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200,
        decoration: BoxDecoration(
            color: const Color(0xff5b5c9b),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
        child: Padding(
            padding: EdgeInsets.only(left: 10, bottom: 10, right: 10),
            child: Stack(
              children: [
                CarouselSlider(
                    items: AdsImage.map((item) => Center(
                            child: Image.asset(item, fit: BoxFit.contain)))
                        .toList(),
                    options: CarouselOptions(
                        height: 190,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 5),
                        viewportFraction: 0.9,
                        onPageChanged: (index, reason) {
                          setState(() {
                            activeIndex = index;
                          });
                        })),
                Container(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 5),
                      child: buildIndicator(),
                    )),
                Container(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5, right: 5),
                    child: Container(
                        alignment: Alignment.center,
                        height: 20,
                        width: 50,
                        decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(10)),
                        child: GestureDetector(
                          child: Text("MORE",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                color: const Color(0x4d000000),
                                fontWeight: FontWeight.w700,
                              )),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchPage()));
                          },
                        )),
                  ),
                )
              ],
            )));
  }

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: activeIndex,
        count: AdsImage.length,
        effect: WormEffect(
            dotHeight: 10,
            dotWidth: 10,
            activeDotColor: Color(0xffffffff),
            dotColor: Color(0xffb3b3b3)),
      );
}
