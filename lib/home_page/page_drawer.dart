import 'package:flutter/material.dart';
import 'package:planner_demo/event_page/event_page.dart';
import 'package:planner_demo/home_page/click_icon.dart';
import 'package:planner_demo/journey_page/journey_page.dart';
import 'package:planner_demo/page_bottombar.dart';
import 'package:planner_demo/planner_page/planner_page.dart';
import 'package:planner_demo/profile_page/profile_page.dart';
import 'package:planner_demo/review_page/review_page.dart';
import 'package:planner_demo/reward_page/reward_page.dart';
import 'package:planner_demo/save_page/save_page.dart';
import 'package:planner_demo/setting_page/setting_page.dart';
import 'package:planner_demo/support_page/support_page.dart';

class PageDrawer extends StatefulWidget {
  const PageDrawer({Key? key}) : super(key: key);

  @override
  State<PageDrawer> createState() => _PageDrawerState();
}

class _PageDrawerState extends State<PageDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.transparent,
      child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), bottomLeft: Radius.circular(30)),
          child: Drawer(
            backgroundColor: const Color(0xffffffff).withOpacity(0.90),
            child: ListView(
              children: [
                Column(
                  children: [
                    SizedBox(height: 20),
                    CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage('assets/images/Profile.jpg'),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Hello Travelers",
                      style: TextStyle(
                        fontFamily: 'Cordia New',
                        fontSize: 25,
                        color: const Color(0xff655c98),
                        shadows: const [
                          Shadow(
                            color: Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    //Follower Box
                    Container(
                      height: 50,
                      width: 240,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: const Color(0xffffffff),
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                                color: const Color(0xff655c98),
                                offset: Offset(0, 3),
                                blurRadius: 6)
                          ]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 70,
                                height: 15,
                                child: Text(
                                  "100",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 15,
                                      color: const Color(0xff655c98),
                                      fontWeight: FontWeight.w700,
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                width: 70,
                                height: 15,
                                child: Text(
                                  "100",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 15,
                                      color: const Color(0xff655c98),
                                      fontWeight: FontWeight.w700,
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                width: 70,
                                height: 15,
                                child: Text(
                                  "100",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 15,
                                      color: const Color(0xff655c98),
                                      fontWeight: FontWeight.w700,
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 70,
                                height: 10,
                                child: Text(
                                  "Follower",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 10,
                                      color: const Color(0xff655c98),
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                width: 70,
                                height: 10,
                                child: Text(
                                  "Following",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 10,
                                      color: const Color(0xff655c98),
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                width: 70,
                                height: 10,
                                child: Text(
                                  "Post",
                                  style: TextStyle(
                                      fontFamily: 'Cordia New',
                                      fontSize: 10,
                                      color: const Color(0xff655c98),
                                      shadows: [
                                        Shadow(
                                            color: const Color(0x29000000),
                                            offset: Offset(0, 3),
                                            blurRadius: 6)
                                      ]),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 50),
                    //Icon Row No.1
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClickIcon(
                          image_location: "assets/images/review.png",
                          name: "REVIEW",
                          LinkPage: () => ReviewPage(),
                        ),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/planner.png",
                            name: "PLANNER",
                            LinkPage: () =>
                                BottomNavBar(LinkPage: () => PlannerPage())),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/marker.png",
                            name: "JOURNEY",
                            LinkPage: () => JourneyPage())
                      ],
                    ),
                    SizedBox(height: 30),
                    //Icon Row No.2
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClickIcon(
                            image_location: "assets/images/bookmark.png",
                            name: "SAVE",
                            LinkPage: () => SavePage()),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/user.png",
                            name: "PROFILE",
                            LinkPage: () => ProfilePage()),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/trophy.png",
                            name: "REWARD",
                            LinkPage: () => RewardPage())
                      ],
                    ),
                    SizedBox(height: 30),
                    //Icon Row No.3
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClickIcon(
                            image_location: "assets/images/rocket-lunch.png",
                            name: "EVENT",
                            LinkPage: () => EventPage()),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/settings.png",
                            name: "SETTING",
                            LinkPage: () => SettingPage()),
                        SizedBox(width: 50),
                        ClickIcon(
                            image_location: "assets/images/globe.png",
                            name: "SUPPORT",
                            LinkPage: () => SupportPage()),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
