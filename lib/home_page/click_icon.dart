import 'package:flutter/material.dart';

class ClickIcon extends StatefulWidget {
  final String image_location;
  final String name;
  Widget Function() LinkPage;

  ClickIcon(
      {Key? key,
      required this.image_location,
      required this.name,
      required this.LinkPage})
      : super(key: key);

  @override
  State<ClickIcon> createState() => _ClickIconState();
}

class _ClickIconState extends State<ClickIcon> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(widget.image_location),
                  fit: BoxFit.fill,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.57), BlendMode.dstIn))),
          child: ListTile(onTap: (() {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => widget.LinkPage()));
          })),
        ),
        SizedBox(height: 5),
        Text(
          widget.name,
          style: TextStyle(
              fontFamily: 'Segoe UI',
              fontSize: 8,
              color: const Color(0xe5655c98),
              fontWeight: FontWeight.w700,
              shadows: [
                Shadow(
                    color: const Color(0x29000000),
                    offset: Offset(0, 3),
                    blurRadius: 6)
              ]),
        )
      ],
    );
  }
}
