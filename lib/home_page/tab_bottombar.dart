import 'package:flutter/material.dart';
import 'package:planner_demo/Notification_page/notification_page.dart';
import 'package:planner_demo/home_page/home_page.dart';
import 'package:planner_demo/home_page/page_drawer.dart';
import 'package:planner_demo/inbox_page/inbox_page.dart';
import 'package:planner_demo/planner_page/planner_page.dart';
import 'package:planner_demo/search_page/search_page.dart';

class TabBottomBar extends StatefulWidget {
  const TabBottomBar({Key? key}) : super(key: key);

  @override
  State<TabBottomBar> createState() => _TabBottomBarState();
}

class _TabBottomBarState extends State<TabBottomBar> {
  int _selectedTabIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  List _pages = [
    HomePage(),
    InboxPage(),
    SearchPage(),
    NotificationPage(),
  ];

  _changeIndex(int index) {
    index == 4
        ? _scaffoldkey.currentState?.openEndDrawer()
        : setState(() {
            _selectedTabIndex = index;
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      endDrawer: PageDrawer(),
      body: Center(child: _pages[_selectedTabIndex]),
      bottomNavigationBar: bottomNavigationBar,
    );
  }

  Widget get bottomNavigationBar {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                  color: const Color(0xff655c98),
                  offset: Offset(0, 3),
                  blurRadius: 6)
            ]),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          child: BottomNavigationBar(
              currentIndex: _selectedTabIndex,
              onTap: _changeIndex,
              type: BottomNavigationBarType.fixed,
              selectedItemColor: Color(0xff655c98),
              unselectedItemColor: Color(0x29000000),
              showSelectedLabels: false,
              showUnselectedLabels: false,
              items: [
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/home.png",
                      height: 20,
                      fit: BoxFit.cover,
                    ),
                    label: ""),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/chat.png",
                      height: 20,
                      fit: BoxFit.cover,
                    ),
                    label: ""),
                BottomNavigationBarItem(
                    icon: Image.asset("assets/images/search.png",
                        height: 20, fit: BoxFit.cover),
                    label: ""),
                BottomNavigationBarItem(
                    icon: Image.asset("assets/images/bell.png",
                        height: 20, fit: BoxFit.cover),
                    label: ""),
                BottomNavigationBarItem(
                    icon: CircleAvatar(
                        radius: 16,
                        backgroundColor: Color(0xff655c98),
                        child: CircleAvatar(
                            radius: 15,
                            backgroundImage:
                                AssetImage('assets/images/Profile.jpg'))),
                    label: "")
              ]),
        ));
  }
}
