import 'package:flutter/material.dart';

class HomeFeed extends StatefulWidget {
  const HomeFeed({Key? key}) : super(key: key);

  @override
  State<HomeFeed> createState() => _HomeFeedState();
}

class _HomeFeedState extends State<HomeFeed> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 10,
        itemBuilder: (context, index) => _postView());
  }

  Widget _postView() {
    return Padding(
      padding: EdgeInsets.only(left: 5, top: 5, right: 5),
      child: Container(
        height: 100,
        decoration: BoxDecoration(color: const Color(0xffffffff), boxShadow: [
          BoxShadow(
              color: const Color(0x29000000),
              offset: Offset(0, 3),
              blurRadius: 6)
        ]),
      ),
    );
  }
}
