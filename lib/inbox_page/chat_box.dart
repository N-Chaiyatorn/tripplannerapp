import 'package:flutter/material.dart';
import 'package:planner_demo/inbox_page/chat_profile.dart';

class ChatBox extends StatefulWidget {
  const ChatBox({Key? key}) : super(key: key);

  @override
  State<ChatBox> createState() => _ChatBoxState();
}

class _ChatBoxState extends State<ChatBox> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff2f2f2),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xffffffff),
        leading: const BackButton(color: Color(0xff655c98)),
        title: GestureDetector(
          child: Row(
            children: [
              CircleAvatar(
                radius: 15,
                backgroundImage: AssetImage('assets/images/Profile.jpg'),
              ),
              SizedBox(width: 5),
              Text('Hello Travelers',
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: const Color(0xff655c98),
                      fontWeight: FontWeight.w700)),
            ],
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ChatProfile()));
          },
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.call, color: Color(0xff655c98))),
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.videocam, color: Color(0xff655c98)))
        ],
      ),
    );
  }
}
