import 'package:flutter/material.dart';
import 'package:planner_demo/inbox_page/chat_tab.dart';
import 'package:planner_demo/inbox_page/new_message.dart';

class InboxPage extends StatefulWidget {
  const InboxPage({Key? key}) : super(key: key);

  @override
  State<InboxPage> createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff2f2f2),
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xffffffff),
        title: Text('Chats',
            style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 25,
                color: const Color(0xff655c98),
                fontWeight: FontWeight.w700)),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NewMessage()));
              },
              icon: Icon(Icons.chat_outlined, color: Color(0xff655c98)))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
                padding:
                    EdgeInsets.only(left: 15, top: 20, right: 15, bottom: 10),
                child: Container(
                    height: 35,
                    decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          BoxShadow(
                              color: const Color(0x29000000),
                              offset: Offset(0, 3),
                              blurRadius: 6),
                        ]),
                    child: TextFormField(
                        textAlign: TextAlign.start,
                        controller: _controller,
                        onChanged: (value) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Padding(
                            padding: EdgeInsets.all(0),
                            child: Icon(Icons.search, color: Color(0xff655c98)),
                          ),
                          hintText: "Search",
                          hintStyle: TextStyle(
                              fontFamily: 'Segoe UI',
                              fontSize: 15,
                              color: const Color(0xff655c98)),
                          suffixIcon: _controller.text.isEmpty
                              ? null
                              : IconButton(
                                  icon: Icon(Icons.clear),
                                  padding: const EdgeInsets.only(top: 0),
                                  onPressed: () {
                                    setState(() {
                                      _controller.clear();
                                    });
                                  }),
                        )))),
            ChatTab(),
            ChatTab()
          ],
        ),
      ),
    );
  }
}
