import 'package:flutter/material.dart';
import 'package:planner_demo/inbox_page/chat_box.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ChatTab extends StatefulWidget {
  const ChatTab({Key? key}) : super(key: key);

  @override
  State<ChatTab> createState() => _ChatTabState();
}

class _ChatTabState extends State<ChatTab> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 2, bottom: 2),
        child: Slidable(
            endActionPane: ActionPane(motion: ScrollMotion(), children: [
              SlidableAction(
                onPressed: ((context) {}),
                backgroundColor: Color(0xfff2f2f2),
                foregroundColor: Color(0xff655c98),
                label: 'Mute',
              ),
              SlidableAction(
                onPressed: ((context) {}),
                backgroundColor: Colors.red,
                label: 'Delete',
              ),
            ]),
            child: GestureDetector(
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(),
                  child: Row(
                    children: [
                      SizedBox(width: 15),
                      CircleAvatar(
                        radius: 25,
                        backgroundImage:
                            AssetImage('assets/images/Profile.jpg'),
                      ),
                      SizedBox(width: 15),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Hello Travelers",
                              style: TextStyle(
                                  fontFamily: 'Cordia New',
                                  fontSize: 15,
                                  color: const Color(0xff655c98),
                                  fontWeight: FontWeight.w700)),
                          Text("Sent 10h ago",
                              style: TextStyle(
                                  fontFamily: 'Cordia New',
                                  fontSize: 10,
                                  color: const Color(0xff655c98)))
                        ],
                      )
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChatBox()));
                })));
  }
}
