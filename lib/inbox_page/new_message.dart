import 'package:flutter/material.dart';

class NewMessage extends StatefulWidget {
  const NewMessage({Key? key}) : super(key: key);

  @override
  State<NewMessage> createState() => _NewMessageState();
}

class _NewMessageState extends State<NewMessage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff2f2f2),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          leading: const BackButton(color: Color(0xff655c98)),
          title: Text('New Message',
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 25,
                  color: const Color(0xff655c98),
                  fontWeight: FontWeight.w700)),
          centerTitle: true,
        ));
  }
}
