import 'package:flutter/material.dart';
import 'package:planner_demo/home_page/home_page.dart';
import 'package:planner_demo/profile_page/tab_topbar.dart';
import 'package:planner_demo/profile_page/view_follow.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff2f2f2),
      body: SingleChildScrollView(
          child: Stack(children: [
        //Header Background
        Container(
          height: 200,
          decoration: BoxDecoration(
              color: const Color(0xe5dcd8ef),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30))),
        ),

        //Feed Background
        Padding(
            padding: EdgeInsets.fromLTRB(30, 110, 30, 10),
            child: Container(
                height: 550,
                decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [
                      BoxShadow(
                          color: Color(0x38000000),
                          offset: Offset(0, 3),
                          blurRadius: 6)
                    ]),
                child: TabTopBar())),

        //Profile Information
        Padding(
            padding: EdgeInsets.fromLTRB(75, 80, 30, 100),
            child: Container(
              height: 90,
              decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: const [
                    BoxShadow(
                        color: const Color(0xff655c98),
                        offset: Offset(0, 3),
                        blurRadius: 6)
                  ]),
            )),

        //Profile Picture
        Padding(
            padding: EdgeInsets.fromLTRB(30, 80, 30, 100),
            child: Container(
              height: 90,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(45),
                          boxShadow: [
                            BoxShadow(
                                color: const Color(0xff655c98),
                                offset: Offset(0, 3),
                                blurRadius: 6)
                          ]),
                      child: CircleAvatar(
                        radius: 45,
                        backgroundImage:
                            AssetImage('assets/images/Profile.jpg'),
                      )),
                  Padding(
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: Container(
                        height: 80,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                        child: Text('Hello Travelers',
                                            style: TextStyle(
                                                fontFamily: 'Cordia New',
                                                fontSize: 16,
                                                color: const Color(0xff655c98),
                                                fontWeight: FontWeight.w700))),
                                    GestureDetector(
                                        child: Container(
                                          width: 50,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              color: const Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              border: Border.all(
                                                  width: 1,
                                                  color:
                                                      const Color(0xff707070))),
                                          child: Text("Edit",
                                              style: TextStyle(
                                                  fontFamily: 'Cordia New',
                                                  fontSize: 14,
                                                  color:
                                                      const Color(0xff655c98))),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      HomePage()));
                                        }),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  ViewFollow(
                                    number: 1000,
                                    name: "Follower",
                                    LinkPage: () => HomePage(),
                                  ),
                                  SizedBox(width: 4),
                                  ViewFollow(
                                    number: 100,
                                    name: "Following",
                                    LinkPage: () => HomePage(),
                                  ),
                                  ViewFollow(
                                    number: 1000,
                                    name: "Post",
                                    LinkPage: () => HomePage(),
                                  )
                                ],
                              ),
                            ]),
                      )),
                ],
              ),
            )),
      ])),
    );
  }
}
