import 'package:flutter/material.dart';
import 'package:planner_demo/profile_page/tab/tab_content.dart';
import 'package:planner_demo/profile_page/tab/tab_planner.dart';
import 'package:planner_demo/profile_page/tab/tab_review.dart';

class TabTopBar extends StatefulWidget {
  const TabTopBar({Key? key}) : super(key: key);

  @override
  State<TabTopBar> createState() => _TabTopBarState();
}

class _TabTopBarState extends State<TabTopBar> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xffffffff),
            elevation: 1,
            bottom: TabBar(
              tabs: [
                Tab(
                    icon: Image.asset("assets/images/apps.png",
                        color: Colors.black.withOpacity(0.57),
                        height: 20,
                        fit: BoxFit.cover)),
                Tab(
                    icon: Image.asset("assets/images/review.png",
                        color: Colors.black.withOpacity(0.57),
                        height: 20,
                        fit: BoxFit.cover)),
                Tab(
                    icon: Image.asset("assets/images/planner.png",
                        color: Colors.black.withOpacity(0.57),
                        height: 20,
                        fit: BoxFit.cover))
              ],
              indicatorColor: Color(0xff655c98),
            ),
          ),
          body: TabBarView(children: [TabContent(), TabPlanner(), TabReview()]),
        ));
  }
}
