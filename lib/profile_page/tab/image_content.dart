import 'package:flutter/material.dart';

class ImageContent extends StatefulWidget {
  final String image_location;
  final double size;
  const ImageContent(
      {Key? key, required this.image_location, required this.size})
      : super(key: key);

  @override
  State<ImageContent> createState() => _ImageContentState();
}

class _ImageContentState extends State<ImageContent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: widget.size,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(widget.image_location), fit: BoxFit.cover)),
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Image(image: AssetImage(widget.image_location))));
      },
    );
  }
}
