import 'package:flutter/material.dart';
import 'package:planner_demo/profile_page/tab/image_content.dart';

class TabContent extends StatefulWidget {
  const TabContent({Key? key}) : super(key: key);

  @override
  State<TabContent> createState() => _TabContentState();
}

class _TabContentState extends State<TabContent> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      primary: false,
      slivers: [
        SliverPadding(
            padding: const EdgeInsets.all(1),
            sliver: SliverGrid.count(
              crossAxisCount: 3,
              mainAxisSpacing: 1,
              crossAxisSpacing: 1,
              childAspectRatio: 1 / 1,
              children: [
                ImageContent(
                    image_location: "assets/images/Content/1.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/2.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/3.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/4.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/5.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/6.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/7.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/8.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/9.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/10.jpg", size: 35),
                ImageContent(
                    image_location: "assets/images/Content/11.jpg", size: 35),
              ],
            )),
      ],
    );
  }
}
