import 'package:flutter/material.dart';
import 'package:planner_demo/home_page/home_page.dart';

class ViewFollow extends StatefulWidget {
  final String name;
  var number;
  Widget Function() LinkPage;
  ViewFollow(
      {Key? key,
      required this.name,
      required this.number,
      required this.LinkPage})
      : super(key: key);

  @override
  State<ViewFollow> createState() => _ViewFollowState();
}

class _ViewFollowState extends State<ViewFollow> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          width: 70,
          height: 50,
          child: Column(
            children: [
              Text(
                widget.number.toString(),
                style: TextStyle(
                    fontFamily: 'Cordia New',
                    fontSize: 20,
                    color: const Color(0xff655c98),
                    fontWeight: FontWeight.w700,
                    shadows: [
                      Shadow(
                          color: const Color(0x29000000),
                          offset: Offset(0, 3),
                          blurRadius: 6)
                    ]),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 3),
              Text(
                widget.name,
                style: TextStyle(
                    fontFamily: 'Cordia New',
                    fontSize: 15,
                    color: const Color(0xff655c98),
                    shadows: [
                      Shadow(
                          color: const Color(0x29000000),
                          offset: Offset(0, 3),
                          blurRadius: 6)
                    ]),
                textAlign: TextAlign.center,
              ),
            ],
          )),
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => widget.LinkPage()));
      },
    );
  }
}
