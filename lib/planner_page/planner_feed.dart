import 'package:flutter/material.dart';

class PlannerFeed extends StatefulWidget {
  const PlannerFeed({Key? key}) : super(key: key);

  @override
  State<PlannerFeed> createState() => _PlannerFeedState();
}

class _PlannerFeedState extends State<PlannerFeed> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: Container(
        height: 85,
        decoration: BoxDecoration(
            color: const Color(0xffffffff),
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x29000000),
                  offset: Offset(0, 3),
                  blurRadius: 6)
            ]),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            children: [
              Container(
                width: 70,
                decoration: BoxDecoration(
                    border: Border(right: BorderSide(color: Colors.black))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "2",
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          fontSize: 25,
                          color: const Color(0xff655c98),
                          fontWeight: FontWeight.w700),
                    ),
                    Text(
                      "DAYS",
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          fontSize: 12,
                          color: const Color(0xff655c98),
                          fontWeight: FontWeight.w400),
                    ),
                    Text(
                      "TO GO",
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          fontSize: 18,
                          color: const Color(0xff655c98),
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "APRIL 4 - APRIL 8",
                    style: TextStyle(
                        fontFamily: 'Cordia New',
                        fontSize: 16,
                        color: const Color(0xcc655c98),
                        fontWeight: FontWeight.w700),
                  ),
                  Text("Bangkok",
                      style: TextStyle(
                          fontFamily: 'Cordia New',
                          fontSize: 15,
                          color: const Color(0xcc655c98),
                          fontWeight: FontWeight.w700)),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 10,
                        backgroundImage:
                            AssetImage('assets/images/Profile.jpg'),
                      ),
                      SizedBox(width: 1),
                      CircleAvatar(
                          radius: 10,
                          backgroundImage:
                              AssetImage('assets/images/Content/1.jpg')),
                      SizedBox(width: 1),
                      Text("Share with 2",
                          style: TextStyle(
                              fontFamily: 'Cordia New',
                              fontSize: 15,
                              color: const Color(0xcc655c98)))
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
