import 'package:flutter/material.dart';
import 'package:planner_demo/home_page/home_page.dart';
import 'package:planner_demo/planner_page/planner_button.dart';
import 'package:planner_demo/planner_page/planner_feed.dart';
import 'package:planner_demo/planner_page/planner_recommend.dart';
import 'package:planner_demo/review_page/review_page.dart';
import 'package:planner_demo/search_page/search_page.dart';

class PlannerPage extends StatefulWidget {
  const PlannerPage({Key? key}) : super(key: key);

  @override
  State<PlannerPage> createState() => _PlannerPageState();
}

class _PlannerPageState extends State<PlannerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff2f2f2),
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xffffffff),
        title: Text('LOGO',
            style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 25,
                color: const Color(0xff655c98),
                fontWeight: FontWeight.w700)),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            height: 150,
            decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x38000000),
                      offset: Offset(0, 0),
                      blurRadius: 5)
                ]),
            child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    PlannerButton(
                        image_location: "assets/images/planner.png",
                        name: "Create Plan",
                        LinkPage: () => HomePage()),
                    PlannerButton(
                        image_location: "assets/images/chat.png",
                        name: "Drafts",
                        LinkPage: () => SearchPage()),
                    PlannerButton(
                        image_location: "assets/images/bookmark.png",
                        name: "Favorite",
                        LinkPage: () => ReviewPage())
                  ],
                )),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15, bottom: 15),
            child: Column(
              children: [
                Text(
                  "Upcoming plan",
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: Color(0xff655c98),
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 15,
                ),
                PlannerFeed(),
                PlannerFeed(),
              ],
            ),
          ),
          PlannerRecommend(),
        ],
      )),
    );
  }
}
