import 'package:flutter/material.dart';

class PlannerButton extends StatefulWidget {
  final String image_location;
  final String name;
  Widget Function() LinkPage;
  PlannerButton(
      {Key? key,
      required this.image_location,
      required this.name,
      required this.LinkPage})
      : super(key: key);

  @override
  State<PlannerButton> createState() => _PlannerButtonState();
}

class _PlannerButtonState extends State<PlannerButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 90,
        width: 105,
        decoration: BoxDecoration(
            color: const Color(0xcce1ddf4),
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x21000000),
                  offset: Offset(0, 3),
                  blurRadius: 6)
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
                image: AssetImage(widget.image_location),
                height: 30,
                color: Colors.black.withOpacity(0.57)),
            SizedBox(height: 10),
            Text(
              widget.name,
              style: TextStyle(
                  fontFamily: 'Segoe UI',
                  fontSize: 17,
                  color: const Color(0xff655c98)),
            )
          ],
        ),
      ),
      onTap: (() {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => widget.LinkPage()));
      }),
    );
  }
}
