import 'package:flutter/material.dart';

class PlannerRecommend extends StatefulWidget {
  const PlannerRecommend({Key? key}) : super(key: key);

  @override
  State<PlannerRecommend> createState() => _PlannerRecommendState();
}

class _PlannerRecommendState extends State<PlannerRecommend> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 390,
      decoration: BoxDecoration(
          color: const Color(0xe58982b1),
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(
                color: const Color(0xe5bbb3e9),
                offset: Offset(0, 0),
                blurRadius: 5)
          ]),
    );
  }
}
