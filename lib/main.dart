import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:planner_demo/login_page/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Planner App Demo',
        theme: ThemeData(
          primarySwatch: Colors.purple,
        ),
        home: AnimatedSplashScreen(
          duration: 3000,
          splash: Icons.home,
          nextScreen: const LogIn(),
          splashTransition: SplashTransition.fadeTransition,
          backgroundColor: const Color(0xAA5f5496),
          pageTransitionType: PageTransitionType.fade,
        ));
  }
}
